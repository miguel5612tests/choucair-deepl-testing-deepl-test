package pages;
                               
import org.openqa.selenium.By;

public class AutomatizacionTraductorDeeplTestingPaginaPrincipalPage {
    public static final By TXT_CAMPOTEXTOTRADUCIR = By.xpath("//*[@id="dl_translator"]/div[3]/div[3]/div[1]/div[2]/div[2]/textarea");
    public static final By TXT_CAMPOTEXTOTRADUCCION = By.xpath("v//*[@id="dl_translator"]/div[3]/div[3]/div[3]/div[4]/div[1]/textarea");
}