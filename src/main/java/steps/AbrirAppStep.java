package steps;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.assertj.core.api.SoftAssertions;
import utils.MetodosJSON;
import utils.PersonalFunctions;
import java.util.logging.Level;
import java.util.logging.Logger;
import pages.*;

public class AbrirAppStep extends UIInteractionSteps {
    Logger logger=Logger.getLogger("AbrirAppStep");
    SoftAssertions softAssertions = new SoftAssertions();
    MetodosJSON metodosJSON;
    PersonalFunctions personalFunctions;
    
    @Step
    public void abrirApp(String url)
    {
        try
        {
            metodosJSON.abrirSitioWeb(url);
        }
        catch (Exception e)
        {
            logger.log(Level.INFO, "OCURRIO UN ERROR: ",e);
            softAssertions.fail(e.getMessage());
            softAssertions.assertAll();
            getDriver().quit();
        }
    }
}