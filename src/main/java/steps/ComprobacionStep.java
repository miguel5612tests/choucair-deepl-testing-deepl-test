package steps;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.assertj.core.api.SoftAssertions;
import utils.MetodosJSON;
import utils.PersonalFunctions;
import java.util.logging.Level;
import java.util.logging.Logger;
import pages.*;

public class ComprobacionStep extends UIInteractionSteps {
    Logger logger=Logger.getLogger("ComprobacionStep");
    SoftAssertions softAssertions = new SoftAssertions();
    MetodosJSON metodosJSON;
    PersonalFunctions personalFunctions;
    
    @Step
    public void comprobacion(String value)
    {
        try
        {
            metodosJSON.validarMensajeElemento(AutomatizacionTraductorDeeplTestingPaginaPrincipalPage.TXT_CAMPOTEXTOTRADUCCION, value);
        }
        catch (Exception e)
        {
            logger.log(Level.INFO, "OCURRIO UN ERROR: ",e);
            softAssertions.fail(e.getMessage());
            softAssertions.assertAll();
            getDriver().quit();
        }
    }
}