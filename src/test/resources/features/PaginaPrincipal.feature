#Author: TurboCycle

@PaginaPrincipal
Feature: El usuario X ingresa, escribe la palabra Hola y espera la traduccion al ingles Hi
Scenario Outline: Traduccion
    Given El Usuario Abre La App
    |<url>|
    When El Usuario Escribira La Palabra Hola
    |<txt_CampoTextoTraducir>|
    Then El Usuario Comprueba Que Hola Haya Sido Traducido Como Hi
    |<txt_CampoTextoTraduccion>|
  Examples:
   |url|txt_CampoTextoTraducir|txt_CampoTextoTraduccion|
   |https://www.deepl.com/translator|Hola|Hi|

