package runners;


import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import utils.BeforeSuite;
import utils.DataToFeature;

@CucumberOptions(features = {"src/test/resources/features/PaginaPrincipal.feature"},
        tags = "@PaginaPrincipal",
        glue = "definitions",
        snippets = SnippetType.CAMELCASE )
@RunWith(RunnerPersonalizado.class)
public class PaginaPrincipalRunner
{
    private PaginaPrincipalRunner() {}
    @BeforeSuite
    public static void test() throws InvalidFormatException, IOException
    {
        DataToFeature.overrideFeatureFiles("src/test/resources/features/PaginaPrincipal.feature");
    }
}