package definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;
import java.util.List;
import steps.*;

public class PaginaPrincipalDefinition {

    @Steps
    AbrirAppStep abrirAppStep;
    PalabraATraducirStep palabraATraducirStep;
    ComprobacionStep comprobacionStep;

    @Given("^El Usuario Abre La App$")
    public void elUsuarioAbreLaApp(List<String> datos) {
          abrirAppStep.abrirApp(datos.get(0));
    }

    @When("^El Usuario Escribira La Palabra Hola$")
    public void elUsuarioEscribiraLaPalabraHola(List<String> datos) {
          palabraATraducirStep.palabraATraducir(datos.get(0));
    }

    @Then("^El Usuario Comprueba Que Hola Haya Sido Traducido Como Hi$")
    public void elUsuarioCompruebaQueHolaHayaSidoTraducidoComoHi(List<String> datos) {
          comprobacionStep.comprobacion(datos.get(0));
    }

}